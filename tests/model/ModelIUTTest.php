<?php
use PHPUnit\Framework\TestCase;

require_once(__DIR__. DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, array("../..", "lib","File.php")));
require_once (File::build_path(array("model","ModelIUT.php")));

class ModelIUTTest extends TestCase
{
    public function loadSomeIUT()
    {
        return array(
            new ModelIUT('1', 'Iut de Montpellier', '13339', '99 Avenue d\'Occitanie', 'https://iut-montpellier-sete.edu.umontpellier.fr/', '04 99 58 51 80', 'none'),
            new ModelIUT('4', 'Iut d\'Amiens', '32933', 'Avenue des facultés Le Bailly', 'www.iut-amiens.fr', '03 22 53 40 81', 'none'),
            new ModelIUT('8', 'Iut de Bordeaux', '12679', '15, rue Naudet - CS 10207 ', 'https://www.iut.u-bordeaux.fr/general/', '05 56 84 57 87', 'none')
        );
    }

    public function testGetAllIUTs()
    {
        $tab_articles = $this->loadSomeIUT();
        $tab_a = ModelIUT::getAllIUTs();
        $this->assertSame($tab_a[0]->getIdIUT(), $tab_articles[0]->getIdIUT());
        $this->assertSame($tab_a[3]->getIdIUT(), $tab_articles[1]->getIdIUT());
        $this->assertSame($tab_a[7]->getIdIUT(), $tab_articles[2]->getIdIUT());
        $this->assertSame($tab_a[0]->getNomIUT(), $tab_articles[0]->getNomIUT());
        $this->assertSame($tab_a[3]->getNomIUT(), $tab_articles[1]->getNomIUT());
        $this->assertSame($tab_a[7]->getNomIUT(), $tab_articles[2]->getNomIUT());
        $this->assertSame($tab_a[0]->getIdVille(), $tab_articles[0]->getIdVille());
        $this->assertSame($tab_a[3]->getIdVille(), $tab_articles[1]->getIdVille());
        $this->assertSame($tab_a[7]->getIdVille(), $tab_articles[2]->getIdVille());
        $this->assertSame($tab_a[0]->getAdresseIUT(), $tab_articles[0]->getAdresseIUT());
        $this->assertSame($tab_a[3]->getAdresseIUT(), $tab_articles[1]->getAdresseIUT());
        $this->assertSame($tab_a[7]->getAdresseIUT(), $tab_articles[2]->getAdresseIUT());
        $this->assertSame($tab_a[0]->getSiteIUT(), $tab_articles[0]->getSiteIUT());
        $this->assertSame($tab_a[3]->getSiteIUT(), $tab_articles[1]->getSiteIUT());
        $this->assertSame($tab_a[7]->getSiteIUT(), $tab_articles[2]->getSiteIUT());
        $this->assertSame($tab_a[0]->getTelephoneIUT(), $tab_articles[0]->getTelephoneIUT());
        $this->assertSame($tab_a[3]->getTelephoneIUT(), $tab_articles[1]->getTelephoneIUT());
        $this->assertSame($tab_a[7]->getTelephoneIUT(), $tab_articles[2]->getTelephoneIUT());
    }

    public function testGetIUTById()
    {
        $tab_articles = $this->loadSomeIUT();
        $article1 = ModelIUT::getIUTById(1);
        $this->assertSame($article1->getIdIUT(), $tab_articles[0]->getIdIUT());
        $this->assertSame($article1->getNomIUT(), $tab_articles[0]->getNomIUT());
        $this->assertSame($article1->getIdVille(), $tab_articles[0]->getIdVille());
        $this->assertSame($article1->getAdresseIUT(), $tab_articles[0]->getAdresseIUT());
        $this->assertSame($article1->getSiteIUT(), $tab_articles[0]->getSiteIUT());
        $this->assertSame($article1->getTelephoneIUT(), $tab_articles[0]->getTelephoneIUT());
    }
}