<?php
use PHPUnit\Framework\TestCase;

require_once(__DIR__. DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, array("../..", "lib","File.php")));
require_once (File::build_path(array("model","ModelAdministrateur.php")));
require_once (File::build_path(array("lib", "Security.php")));

class ModelAdministrateurTest extends TestCase
{
    public function loadAdministrateurs()
    {
        return array(
            new ModelAdministrateur('test', 'test'),
            new ModelAdministrateur('admin', 'admin')
        );
    }

    public function testGetAllAdministrateurs()
    {
        $tab_administrateurs = $this->loadAdministrateurs();
        $_SESSION['login'] = 'test';
        $tab_v = ModelAdministrateur::getAllAdministrateurs();
        $this->assertSame($tab_v[0]->getLogin(), $tab_administrateurs[0]->getLogin());
        $this->assertSame($tab_v[0]->getMdp(), $tab_administrateurs[0]->getMdp());
        $this->assertSame($tab_v[1]->getLogin(), $tab_administrateurs[1]->getLogin());
        $this->assertSame($tab_v[1]->getMdp(), Security::chiffrer($tab_administrateurs[1]->getMdp()));
    }

    public function testGetAdministrateurById()
    {
        $tab_administrateurs = $this->loadAdministrateurs();
        $_SESSION['login'] = 'test';
        $admin1 = ModelAdministrateur::getAdministrateurById(1);
        $admin4 = ModelAdministrateur::getAdministrateurById(4);
        $this->assertSame($admin1->getLogin(), $tab_administrateurs[0]->getLogin());
        $this->assertSame($admin1->getMdp(), $tab_administrateurs[0]->getMdp());
        $this->assertSame($admin4->getLogin(), $tab_administrateurs[1]->getLogin());
        $this->assertSame($admin4->getMdp(), Security::chiffrer($tab_administrateurs[1]->getMdp()));
    }

    public function testGetAdministrateurByLogin()
    {
        $tab_administrateurs = $this->loadAdministrateurs();
        $_SESSION['login'] = 'test';
        $admin1 = ModelAdministrateur::getAdministrateurByLogin('test');
        $admin4 = ModelAdministrateur::getAdministrateurByLogin('admin');
        $this->assertSame($admin1->getLogin(), $tab_administrateurs[0]->getLogin());
        $this->assertSame($admin1->getMdp(), $tab_administrateurs[0]->getMdp());
        $this->assertSame($admin4->getLogin(), $tab_administrateurs[1]->getLogin());
        $this->assertSame($admin4->getMdp(), Security::chiffrer($tab_administrateurs[1]->getMdp()));
    }
}