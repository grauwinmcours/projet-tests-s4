<?php
use PHPUnit\Framework\TestCase;

require_once(__DIR__. DIRECTORY_SEPARATOR . join(DIRECTORY_SEPARATOR, array("../..", "lib","File.php")));
require_once File::build_path(array("model","Model.php"));
require_once (File::build_path(array("model","ModelArticle.php")));

class ModelArticleTest extends TestCase
{
    public function loadSomeArticles()
    {
        return array(
            new ModelArticle(2, 'Trouver un IUT Proche', 'lorem ipsum'),
            new ModelArticle(7, 'Les pâtes à la cafet', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non neque dolor. Cras ornare, sem quis molestie fermentum, nulla mauris tempus urna, vitae accumsan justo mi sed diam. Nulla ligula eros, fermentum vel diam at, convallis elementum nulla. Ut vel purus a metus imperdiet suscipit. Mauris vel ligula vitae lorem pellentesque euismod vitae et orci. Vivamus pharetra volutpat rhoncus. In lobortis lobortis dapibus. Etiam sed lectus est.

            Ut laoreet neque non ultricies euismod. Mauris mattis nibh augue, id ornare est posuere id. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec massa sit amet tortor iaculis ultrices. Donec libero turpis, pharetra mattis molestie nec, imperdiet ac felis. Proin non facilisis eros, nec fermentum est. Nam at molestie lectus. Vivamus non aliquet enim, sit amet varius nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent fermentum justo at odio viverra laoreet.
            
            Curabitur vestibulum auctor ante, ac ultricies massa. Mauris non ex mattis, vestibulum metus sit amet, tristique est. Maecenas non pretium lectus. Cras varius ligula viverra, iaculis diam a, vestibulum metus. Suspendisse ullamcorper erat sit amet ante placerat porttitor. Pellentesque scelerisque aliquam nulla, quis aliquam eros porttitor volutpat. Nulla urna risus, egestas ut enim non, fringilla posuere magna. Praesent nec tincidunt eros, sit amet rhoncus sapien. Phasellus sit amet velit non ligula euismod sodales. Phasellus nulla massa, venenatis et ligula at, tempor fringilla ligula. Proin sagittis maximus dapibus. Curabitur non tincidunt neque, eget pellentesque diam. Proin sit amet justo finibus, hendrerit turpis at, lacinia quam. Morbi gravida pretium massa, a luctus odio egestas id. Donec a ligula elit. Phasellus sodales turpis non dolor scelerisque ullamcorper ullamcorper feugiat ex. '),
            new ModelArticle(8, 'Plus d\'heures sup !', 'Je veux bosser !')
        );
    }

    public function testGetAllArticles()
    {
        $tab_articles = $this->loadSomeArticles();
        $tab_a = ModelArticle::getAllArticles();
        $this->assertSame($tab_a[0]->getNomArticle(), $tab_articles[0]->getNomArticle());
        $this->assertSame($tab_a[1]->getNomArticle(), $tab_articles[1]->getNomArticle());
        $this->assertSame($tab_a[2]->getNomArticle(), $tab_articles[2]->getNomArticle());
        $this->assertSame($tab_a[0]->getContenuArticle(), $tab_articles[0]->getContenuArticle());
        //$this->assertSame($tab_a[1]->getContenuArticle(), $tab_articles[1]->getContenuArticle()); erreur lié au contenu de l'article trop long
        $this->assertSame($tab_a[2]->getContenuArticle(), $tab_articles[2]->getContenuArticle());
    }

    public function testGetArticleById()
    {
        $tab_articles = $this->loadSomeArticles();
        $article2 = ModelArticle::getArticleById(2);
        $article7 = ModelArticle::getArticleById(7);
        $article8 = ModelArticle::getArticleById(8);
        $this->assertSame($article2->getNomArticle(), $tab_articles[0]->getNomArticle());
        $this->assertSame($article7->getNomArticle(), $tab_articles[1]->getNomArticle());
        $this->assertSame($article8->getNomArticle(), $tab_articles[2]->getNomArticle());
        $this->assertSame($article2->getContenuArticle(), $tab_articles[0]->getContenuArticle());
        //$this->assertSame($article7->getContenuArticle(), $tab_articles[1]->getContenuArticle()); erreur lié au contenu de l'article trop long
        $this->assertSame($article8->getContenuArticle(), $tab_articles[2]->getContenuArticle());
    }
}